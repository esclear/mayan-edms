# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Nurgül Özkan <nurgulozkan@hotmail.com>, 2017
# serhatcan77 <serhat_can@yahoo.com>, 2017
msgid ""
msgstr ""
"Project-Id-Version: Mayan EDMS\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-06-07 21:03-0400\n"
"PO-Revision-Date: 2018-06-08 01:08+0000\n"
"Last-Translator: serhatcan77 <serhat_can@yahoo.com>\n"
"Language-Team: Turkish (Turkey) (http://www.transifex.com/rosarior/mayan-edms/language/tr_TR/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: tr_TR\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#: apps.py:85 permissions_runtime.py:7 settings.py:11
msgid "Common"
msgstr "Ortak"

#: apps.py:90
msgid "Anonymous"
msgstr "Anonim"

#: classes.py:181
msgid "Available attributes: \n"
msgstr "Kullanılabilir özellikler:\n"

#: dashboards.py:7
msgid "Main"
msgstr ""

#: forms.py:25
msgid "Selection"
msgstr "Seçim"

#: generics.py:136
#, python-format
msgid "Unable to transfer selection: %s."
msgstr "Seçim aktarımı yapılamadı: %s."

#: generics.py:160
msgid "Add"
msgstr "Ekle"

#: generics.py:171
msgid "Remove"
msgstr "Çıkar"

#: generics.py:353
#, python-format
msgid "%(object)s not created, error: %(error)s"
msgstr "%(object)s oluşturulamadı, hata: %(error)s"

#: generics.py:364
#, python-format
msgid "%(object)s created successfully."
msgstr "%(object)s başarıyla oluşturuldu."

#: generics.py:393
#, python-format
msgid "%(object)s not deleted, error: %(error)s."
msgstr "%(object)s silinemedi, hata: %(error)s."

#: generics.py:404
#, python-format
msgid "%(object)s deleted successfully."
msgstr "%(object)s başarıyla silindi."

#: generics.py:450
#, python-format
msgid "%(object)s not updated, error: %(error)s."
msgstr "%(object)s güncellenemedi, hata: %(error)s."

#: generics.py:461
#, python-format
msgid "%(object)s updated successfully."
msgstr "%(object)s başarıyla güncellendi."

#: links.py:33
msgid "About this"
msgstr "Bunun hakkında"

#: links.py:36 views.py:61
msgid "Check for updates"
msgstr "Güncellemeleri kontrol et"

#: links.py:40
msgid "User details"
msgstr "Kullanıcı detayları"

#: links.py:44
msgid "Edit details"
msgstr "Detayları düzenle"

#: links.py:47
msgid "Locale profile"
msgstr "Yerel profil ayarı"

#: links.py:51
msgid "Edit locale profile"
msgstr "Yerel profili düzenle"

#: links.py:55
msgid "Source code"
msgstr "Kaynak kodu"

#: links.py:59
msgid "Documentation"
msgstr "Belgeleme"

#: links.py:64 links.py:74
msgid "Errors"
msgstr ""

#: links.py:69
msgid "Clear all"
msgstr ""

#: links.py:78
msgid "Forum"
msgstr "Forum"

#: links.py:82 views.py:164
msgid "License"
msgstr "Lisans"

#: links.py:85 views.py:239
msgid "Other packages licenses"
msgstr "Diğer paket lisansları"

#: links.py:89
msgid "Setup"
msgstr "Ayarlar"

#: links.py:92
msgid "Support"
msgstr "Destek"

#: links.py:96 queues.py:10 views.py:274
msgid "Tools"
msgstr "Araçlar"

#: literals.py:9
msgid ""
"Your database backend is set to use SQLite. SQLite should only be used for "
"development and testing, not for production."
msgstr ""

#: literals.py:18
msgid "Days"
msgstr "Günler"

#: literals.py:19
msgid "Hours"
msgstr "Saatler"

#: literals.py:20
msgid "Minutes"
msgstr "Dakikalar"

#: management/commands/installjavascript.py:15
msgid "Process a specific app."
msgstr ""

#: menus.py:12
msgid "System"
msgstr "Sistem"

#: menus.py:22 models.py:81
msgid "User"
msgstr "Kullanıcı"

#: mixins.py:82
#, python-format
msgid "Operation performed on %(count)d object"
msgstr "İşlem %(count)d nesnesi üzerinde gerçekleştirildi"

#: mixins.py:83
#, python-format
msgid "Operation performed on %(count)d objects"
msgstr "İşlem %(count)d nesneleri üzerinde gerçekleştirildi"

#: mixins.py:254
msgid "Object"
msgstr "Nesne"

#: models.py:24
msgid "Namespace"
msgstr "Alanadı"

#: models.py:35 models.py:55
msgid "Date time"
msgstr "Tarih saat"

#: models.py:37 views.py:209
msgid "Result"
msgstr "Sonuç"

#: models.py:43
msgid "Error log entry"
msgstr ""

#: models.py:44
msgid "Error log entries"
msgstr ""

#: models.py:51
msgid "File"
msgstr "Dosya"

#: models.py:53
msgid "Filename"
msgstr "Dosya adı"

#: models.py:59
msgid "Shared uploaded file"
msgstr "Paylaşılan yüklenen dosya"

#: models.py:60
msgid "Shared uploaded files"
msgstr "Paylaşılan yüklenen dosyalar"

#: models.py:85
msgid "Timezone"
msgstr "Saat dilimi"

#: models.py:88
msgid "Language"
msgstr "Dil"

#: models.py:92
msgid "User locale profile"
msgstr "Kullanıcı yerel ayar profili"

#: models.py:93
msgid "User locale profiles"
msgstr "Kullanıcı yerel ayar profilleri"

#: permissions_runtime.py:10
msgid "View error log"
msgstr ""

#: queues.py:8
msgid "Default"
msgstr "Varsayılan"

#: queues.py:12
msgid "Common periodic"
msgstr "Ortak periyodik"

#: queues.py:16
msgid "Delete stale uploads"
msgstr "Eski yüklemeleri sil"

#: settings.py:15
msgid "Automatically enable logging to all apps."
msgstr "Tüm uygulamalara günlük kaydını otomatik olarak etkinleştirin."

#: settings.py:21
msgid ""
"Time to delay background tasks that depend on a database commit to "
"propagate."
msgstr "Veritabanına bağımlı arka plan görevlerini geciktirme zamanını oluşturmak için kullanılır."

#: settings.py:28
msgid ""
"Filename of the local settings file (just the filename, extension will be "
".py)."
msgstr ""

#: settings.py:36
msgid "An integer specifying how many objects should be displayed per page."
msgstr "Sayfa başına kaç tane nesnenin görüntüleneceğini belirten bir tam sayı."

#: settings.py:42
msgid "A storage backend that all workers can use to share files."
msgstr "Tüm çalışanların dosyaları paylaşmak için kullanabileceği bir depolama alanı."

#: settings.py:53
msgid ""
"Temporary directory used site wide to store thumbnails, previews and "
"temporary files."
msgstr "Geçici dizin, küçük resimleri, önizlemeleri ve geçici dosyaları saklamak için site genelinde kullanılır."

#: settings.py:62
msgid "Enable error logging outside of the system error logging capabilities."
msgstr ""

#: settings.py:69
msgid "Path to the logfile that will track errors during production."
msgstr "Üretim sırasında hataları izleyecek olan günlük dosyasının yolu."

#: validators.py:29
msgid ""
"Enter a valid 'internal name' consisting of letters, numbers, and "
"underscores."
msgstr "Harfler, rakamlar ve alt çizgilerden oluşan geçerli bir 'dahili ad' girin."

#: views.py:49
#, python-format
msgid "The version you are using is outdated. The latest version is %s"
msgstr "Kullandığınız sürüm güncelliğini yitirmiştir. En son sürümü %s"

#: views.py:54
msgid "It is not possible to determine the latest version available."
msgstr ""

#: views.py:58
msgid "Your version is up-to-date."
msgstr "Sürümünüz güncel."

#: views.py:75
msgid "Current user details"
msgstr "Geçerli kullanıcı ayrıntıları"

#: views.py:80
msgid "Edit current user details"
msgstr "Geçerli kullanıcı ayrıntılarını düzenle"

#: views.py:100
msgid "Current user locale profile details"
msgstr "Geçerli kullanıcı yerel ayarları profil ayrıntıları"

#: views.py:107
msgid "Edit current user locale profile details"
msgstr "Geçerli kullanıcı yerel ayarları ayrıntılarını düzenle"

#: views.py:155
msgid "Dashboard"
msgstr "Gösterge Paneli"

#: views.py:173
#, python-format
msgid "Clear error log entries for: %s"
msgstr ""

#: views.py:190
msgid "Object error log cleared successfully"
msgstr ""

#: views.py:208
msgid "Date and time"
msgstr "Tarih ve saat"

#: views.py:213
#, python-format
msgid "Error log entries for: %s"
msgstr ""

#: views.py:257
msgid "Setup items"
msgstr "Kurulum öğeleri"

#: views.py:300
msgid "No action selected."
msgstr "İşlem seçilmedi."

#: views.py:308
msgid "Must select at least one item."
msgstr "En az bir öğe seçmelisiniz."
